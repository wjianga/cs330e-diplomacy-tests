from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    
# -----
# solve
# -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
                     "D Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve_4(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Tokyo\nE Madrid Support D\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE Madrid\n")

    def test_solve_5(self):
        r = StringIO("B Beijing Support A\nA Tokyo Hold\nC London Support B\nD Austin Move Beijing\nE Madrid Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")
    

# ----
# main
# ----

if __name__ == "__main__":
    main()
    
