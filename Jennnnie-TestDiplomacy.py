# Trang Do & Mohammad Aga
# Diplomacy
# TestDiplomacy.py
# Created: 10/21/2022
# Last Modified: 10/21/2022

# Requires Diplomacy.py

import sys
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_eval, diplomacy_solve

class MyUnitTests (TestCase):

    # diplomacy_read
    def test_read_1(self):
        r = StringIO("A Madrid Move London\n")
        w = diplomacy_read(r)
        self.assertEqual(w, [["A"], ["Madrid"], [["Move", "London"]]])

    def test_read_2(self):
        r = StringIO(" ")
        w = diplomacy_read(r)
        self.assertEqual(w, [[],[],[]])

    def test_read_3(self):
        r = StringIO(
            "A Madrid Move Barcelona\n" \
            "B Barcelona Hold\n" \
            "C London Move Barcelona\n" \
            "D Paris Support A\n" \
            "E Austin Support C\n")
        w = diplomacy_read(r)
        self.assertEqual(w, [["A", "B", "C", "D", "E"], ["Madrid", "Barcelona", "London", "Paris", "Austin"], 
        [["Move", "Barcelona"], ["Hold"], ["Move", "Barcelona"], ["Support", "A"], ["Support", "C"]]])

    def test_read_4(self):
        r = StringIO(
            "A Madrid Move Barcelona\n" \
            "B Barcelona Hold\n" \
            "D Paris Support A\n" \
            "E Austin Support C\n"\
            "C London Move Barcelona\n")
        w = diplomacy_read(r)
        self.assertEqual(w, [["A", "B", "C", "D", "E"], ["Madrid", "Barcelona", "London", "Paris", "Austin"], 
        [["Move", "Barcelona"], ["Hold"], ["Move", "Barcelona"], ["Support", "A"], ["Support", "C"]]])

    # diplomacy_print
    def test_print_1(self):
        w = StringIO()
        v = ([], [])
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "")

    def test_print_2(self):
        w = StringIO()
        v = (["A"], ["London"])
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A London\n")

    def test_print_3(self):
        w = StringIO()
        v = (["A", "B", "C", "D"], ["Barcelona", "Madrid", "[dead]", "[dead]"])
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\nC [dead]\nD [dead]\n")

    def test_eval_1(self):
        s = [[],[],[]]
        t = diplomacy_eval(s)
        self.assertEqual(t, ([], []))

    def test_eval_2(self):
        s = [["A"], ["Madrid"], [["Move", "London"]]]
        r = (["A"], ["London"])
        t = diplomacy_eval(s)
        self.assertEqual(t, r)

    def test_eval_3(self):
        s = [["A", "B", "C", "D", "E", "F", "G", "H"], 
        ["Madrid", "Barcelona", "London", "Paris", "Austin", "Dallas", "Houston", "NewYork"], 
        [["Move", "Barcelona"], ["Hold"], ["Move", "Barcelona"], ["Support", "A"], ["Support", "C"],
        ["Support", "C"], ["Move", "Dallas"], ["Support", "A"]]]
        r = (["A", "B", "C", "D", "E", "F", "G", "H"], 
        ["Barcelona", "[dead]", "[dead]", "Paris", "Austin", "[dead]", "[dead]", "NewYork"])
        t = diplomacy_eval(s)
        self.assertEqual(t, r)

    # corner case: empty input
    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")
    
    # corner case: white space input
    def test_solve_2(self):
        r = StringIO(" ")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    # corner case: 1 army
    def test_solve_3(self):
        r = StringIO("A Madrid Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\n")

    # complex case: different numbers of supporters with some nullified
    def test_solve_4(self):
        r = StringIO(
            "A Madrid Move Barcelona\n" \
            "B Barcelona Hold\n" \
            "C London Move Barcelona\n" \
            "D Paris Support A\n" \
            "E Austin Support C\n" \
            "F Dallas Support C\n" \
            "G Houston Move Dallas\n" \
            "H NewYork Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB [dead]\nC [dead]\nD Paris\nE Austin\nF [dead]\nG [dead]\nH NewYork\n")

if __name__ == "__main__": #pragma: no cover
    main()
